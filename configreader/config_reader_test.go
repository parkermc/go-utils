package configreader

import (
	"os"
	"testing"
)

// Simple struct used to test the config loader
type testConfig struct {
	TestString string `json:"test_string"`
}

func TestConfigReaderNoDefault(t *testing.T) {
	config := new(testConfig)        // Create a new struct to load a config as
	config.TestString = "teststring" // Set a string to varify that it loads the config

	ok, err := LoadConfig(config, "tmp/config.json") // Load the config should do nothing in the end as the file isn't there
	if err != nil {                                  // Fail test if eror
		t.Error("Error loading config: ", err)
	}

	if ok {
		t.Error("Error ok value returned true when it should be false")
	}

	err = SaveConfig(config, "tmp/config.json") // Save the config
	if err != nil {                             // Fail test if eror
		t.Error("Error saving config: ", err)
	}

	if _, err = os.Stat("tmp/config.json"); os.IsNotExist(err) { // If the file does not exist fail test
		t.Error("config file tmp/config.json not found")
	}

	config.TestString = "teststring2"               // Change the string to varify that it loads the config
	ok, err = LoadConfig(config, "tmp/config.json") // Load the config with loadDefaults set to true to varify it doesn't change the data
	if err != nil {                                 // Fail test if eror
		t.Error("Error loading config: ", err)
	}

	if !ok {
		t.Error("Error ok value returned false when it should be true")
	}

	if config.TestString != "teststring" {
		t.Errorf("test string without defaults set equals \"%s\" it should be \"teststring\"", config.TestString)
	}
	os.RemoveAll("tmp/") // Remove the config and the folder it was in
}
