.PHONY: help test setup

null  :=
space := $(null) #
GOPATH=$(shell go env GOPATH)

PACKAGES=$(shell go list ./...)
PACKAGES_FULL_PATH=$(GOPATH)/src/$(subst $(space), $(GOPATH)/src/,$(PACKAGES))

help: ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

setup: ## Setup the project
	@echo Setting up the project

	go get ./...

test: ## Text the code with the test files
test: setup
	@echo Running tests

	golint $(PACKAGES)
	go vet $(PACKAGES)

# over 10 is consittered bad code
	gocyclo -top 10 -over 10 $(PACKAGES_FULL_PATH)

	go test $(PACKAGES)
