package utils

import "testing"

func TestHasElem(t *testing.T) {
	arr := []string{"this", "is", "a", "test"}
	if !HasElem(arr, "is") {
		t.Error("HasElem returned false when it should have the element")
	}

	if HasElem(arr, "s") {
		t.Error("HasElem returned true when it shouldn't have the element")
	}

}
